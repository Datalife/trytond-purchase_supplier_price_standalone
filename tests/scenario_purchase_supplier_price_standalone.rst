===========================================
Purchase Supplier Price Standalone Scenario
===========================================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company

Install purchase_supplier_price_standalone::

    >>> config = activate_modules('purchase_supplier_price_standalone')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create party::

    >>> Party = Model.get('party.party')
    >>> party1= Party(name='Party 1')
    >>> party1.save()
    >>> party2= Party(name='Party 2')
    >>> party2.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.save()
    >>> product, = template.products


Create price with template::

    >>> ProductSupplierPrice = Model.get('purchase.product_supplier.price')
    >>> price1 = ProductSupplierPrice()
    >>> price1.unit_price = Decimal('35')
    >>> price1.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.AccessError: You are not allowed to create records of "Product Supplier Price" because they fail on at least one of these rules:
    User in companies - 
    >>> price1.supplier = party1
    >>> price1.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.AccessError: You are not allowed to create records of "Product Supplier Price" because they fail on at least one of these rules:
    User in companies - 
    >>> price1.template = template
    >>> price1.save()
    >>> price1.product_supplier.template == price1.template
    True
    >>> price1.product_supplier.template.name
    'product'
    >>> price1.product_supplier.party == price1.supplier
    True
    >>> price1.product_supplier.party.name
    'Party 1'


Check standar price creation::

    >>> ProductSupplier = Model.get('purchase.product_supplier')
    >>> product_supplier = ProductSupplier()
    >>> product_supplier.party = party2
    >>> product_supplier.product = product
    >>> price2 = product_supplier.prices.new()
    >>> price2.unit_price = Decimal('33')
    >>> price2.product_supplier.party.name
    'Party 2'
    >>> price2.product_supplier.party.name == price2.supplier.name
    True
    >>> price2.product_supplier.product.name
    'product'
    >>> price2.product_supplier.product.name == price2.product.name
    True
    >>> product_supplier.save()
    >>> price2 = ProductSupplierPrice(product_supplier.prices[0].id)
    >>> price2.product_supplier.party.name
    'Party 2'
    >>> price2.product_supplier.party.name == price2.supplier.name
    True
    >>> price2.product_supplier.product.name
    'product'
    >>> price2.product_supplier.product.name == price2.product.name
    True


Check price updating::

    >>> price1 = ProductSupplierPrice(price1.id)
    >>> product_supplier = price1.product_supplier
    >>> price1.supplier = party2
    >>> price1.unit_price = Decimal('31.2')
    >>> price1.save()
    >>> product_supplier != price1.product_supplier
    True
    >>> product_supplier = ProductSupplier(1)
