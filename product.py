# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta


class ProductSupplierPrice(metaclass=PoolMeta):
    __name__ = 'purchase.product_supplier.price'

    product = fields.Function(
        fields.Many2One('product.product', 'Product'),
        'get_product_supplier', setter='set_product')
    template = fields.Function(
        fields.Many2One('product.template', 'Template'),
        'get_product_supplier', setter='set_template')
    supplier = fields.Function(
        fields.Many2One('party.party', 'Supplier'),
        'get_product_supplier', setter='set_product')

    @fields.depends('product_supplier', '_parent_product_supplier.product',
        '_parent_product_supplier.party', '_parent_product_supplier.template')
    def on_change_product_supplier(self):
        if self.product_supplier:
            self.product = self.product_supplier.product
            self.supplier = self.product_supplier.party
            self.template = self.product_supplier.template

    @classmethod
    def get_product_supplier(cls, records, names):

        return {
            'product': {r.id: r.product_supplier.product
                and r.product_supplier.product.id or None for r in records},
            'supplier': {r.id: r.product_supplier.party.id for r in records},
            'template': {r.id: r.product_supplier.template.id for r in records}
        }

    @classmethod
    def create_product_supplier(cls, product, supplier, template):
        pool = Pool()
        ProductSupplier = pool.get('purchase.product_supplier')
        Product = pool.get('product.product')

        if template is None:
            template = Product(product).template

        product_supplier = ProductSupplier(
            product=product,
            party=supplier,
            template=template)
        product_supplier.save()
        return product_supplier

    @classmethod
    def set_product(cls, prices, name, value):
        pass

    @classmethod
    def set_supplier(cls, prices, name, value):
        pass

    @classmethod
    def set_template(cls, prices, name, value):
        pass

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        ProductSupplier = pool.get('purchase.product_supplier')
        for vals in vlist:
            if ((vals.get('product') or vals.get('template'))
                    and vals.get('supplier')
                    and not vals.get('product_supplier', None)):
                domain = []
                if vals.get('product'):
                    domain.append(('product', '=', vals['product']))
                if vals.get('template'):
                    domain.append(('template', '=', vals['template']))
                product_supplier = ProductSupplier.search([
                    domain,
                    ('party', '=', vals['supplier'])])
                if not product_supplier:
                    product_supplier = cls.create_product_supplier(
                        vals.get('product'),
                        vals['supplier'],
                        vals.get('template'))
                    vals['product_supplier'] = product_supplier.id
                else:
                    vals['product_supplier'] = product_supplier[0].id
        return super(ProductSupplierPrice, cls).create(vlist)

    @classmethod
    def write(cls, *args):
        pool = Pool()
        ProductSupplier = pool.get('purchase.product_supplier')

        actions = iter(args)
        to_delete = []
        for records, values in zip(actions, actions):
            for record in records:
                product = values.get('product', record.product)
                supplier = values.get('supplier', record.supplier)
                template = values.get('template', record.template)
                if (product != record.product or supplier != record.supplier
                        or template != record.template):
                    domain = []
                    if product:
                        domain.append(('product', '=', product))
                    if template:
                        domain.append(('template', '=', template))

                    product_supplier = ProductSupplier.search([
                        domain,
                        ('party', '=', supplier)])
                    if product_supplier:
                        values['product_supplier'] = product_supplier[0]
                    else:
                        product_supplier = cls.create_product_supplier(
                            values.get('product'),
                            values['supplier'],
                            values.get('template'))
                        values['product_supplier'] = product_supplier

                    if (len(record.product_supplier.prices) == 1
                            and record.product_supplier.prices[0] == record):
                        to_delete.append(record.product_supplier)

        super(ProductSupplierPrice, cls).write(*args)

        if to_delete:
            # TODO: Bug, Check why records not be deteled?
            ProductSupplier.delete(to_delete)
